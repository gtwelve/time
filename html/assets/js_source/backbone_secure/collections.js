var appCore = appCore || {};

appCore.projectList = appCore.projectList || {} ;

appCore.homeList = appCore.homeList || {} ;

appCore.workunitList = appCore.workunitList || {} ;

appCore.entryList = appCore.entryList || {} ;


$(function () {
	'use strict';
	
	
	appCore.projectList = appCore.baseCollection.extend({
		model : appCore.project,
		initialize: function() {
		},
		url: function() {
			return '/api/project';
		}
	});
	
	
	appCore.homeList = appCore.baseCollection.extend({
		model : appCore.workunit,
		initialize: function() {
		},
		url: '/api/home',
		comparator: function(model) {
			return -(parseInt(model.get('last_started')) * (model.get('running') == 1 ? 10 : 0));
		},
	});
	
	
	appCore.workunitList = appCore.baseCollection.extend({
		fetched: false,
		project: null,
		model : appCore.workunit,
		url: function() {
			return '/api/project/' + this.project.get('id') + '/workunit';
		},
		initialize: function() {
		},
		comparator: function(model) {
			return -(parseInt(model.get('last_started')) * (model.get('running') == 1 ? 10 : 0));
		},
		doFetch: function() {
			this.fetch({
				remove: false,
				success: function() {
					this.fetched = true;
				}.bind(this)
			});
		},
		setProject: function(project) {
			this.project = project;
			return this;
		}
	});
	
	
	appCore.entryList = appCore.baseCollection.extend({
		model : appCore.entry,
		url: function() {
			return '/api/project-tasks/' + this.project.get('id');
		}
	});
	
	
});