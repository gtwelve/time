var appCore = appCore || {};

appCore.titleView = appCore.titleView || {} ;

appCore.projectTitleView = appCore.projectTitleView || {} ;

appCore.projectListItemView = appCore.projectListItemView || {} ;

appCore.homeView = appCore.homeView || {} ;

appCore.newView = appCore.newView || {} ;

appCore.workunitBase = appCore.workunitBase || {} ;

appCore.homeItemView = appCore.homeItemView || {} ;

appCore.workunitView = appCore.workunitView || {} ;

appCore.projectPageView = appCore.projectPageView | {} ;

appCore.workunitListView = appCore.workunitListView || {} ;

appCore.notFoundView = appCore.notFoundView || {} ;



$(function () {
	'use strict';


	appCore.titleView = appCore.baseView.extend({
		tagName:  "div",
		className: "title_view",
		title: null,
		events: {},
		initialize: function() {
			this.setTemplate('#title_block');
		},
		setTitle: function(title) {
			this.title = title;
			return this;
		}
	});


	appCore.projectTitleView = appCore.baseView.extend({
		tagName:  "div",
		className: "project_title_view",
		editing: false,
		events: {
			'click .delete': 'deleteModel',
			'click .newColor': 'newColor',
			'click .edit': 'edit',
			'blur .editing': 'timerSaveEdit'
		},
		initialize: function() {
			this.setTemplate('#project_title_block');
		},
		deleteModel: function(e) {
			e.preventDefault();

			this.model.destroy({success: function() {
				this.destroy();
				window.location.hash = 'home';
			}.bind(this)});
		},
		newColor: function(e) {
			e.preventDefault();

			this.model.save([], {
				method: 'update',
				url: 'api/project/' + this.model.get('id') + '/new-color',
				success: function(model, xhr, options) {
					window.time.view.header.setColor(model.get('color'));
				}
			});
		},
		edit: function(e) {
			e.preventDefault();
			
			this.editing = this.editing == true ? false : true ;
			this.renderView();
			
			if (this.editing == true) {
				this.$el.find('.editing').focus().val(this.model.get('name'));
			}
		},
		timerSaveEdit: function(e) {
			this.model.save({name: e.currentTarget.value}, {success: function() {
				this.editing = false;
				this.renderView();
			}.bind(this)});
		}
	});


	appCore.workunitBase = appCore.baseView.extend({
		timer: null,
		timerStartTime: null,
		timerStartedAt: null,
		toggleTimer: function() {
			this.$el.find('.timer').text(this.timeToHuman(this.model.get('time')));
			
			if (this.model.get('type') == 'manual') { return; }
			
			if (this.model.get('running') == 1) {
				this.timerStartTime = parseInt(this.model.get('time'));
				this.timerStartedAt = new Date().getTime() / 1000;
				this.$el.find('.toggle_timer').html('<i class="fa fa-pause"></i>');
				this.$el.addClass('active_timer');
				if (this.timer == null) {
					this.timer = window.setInterval(this.progressTimer.bind(this), 1000);
				}
				
			} else {
				this.timerStartTime = null;
				this.timerStartedAt = null;
				this.$el.find('.toggle_timer').html('<i class="fa fa-play">');
				this.$el.removeClass('active_timer');
				this.clearTimer();
			}
		},
		progressTimer: function() {
			var nt = parseInt(this.model.get('time'))+1;
			
			if ((nt % 10) == 0) {	// About every 10 seconds calculate the exact time passed beacuse the setInterval is unreliable and is not called every 1000ms
				nt = this.timerStartTime + Math.round((new Date().getTime() / 1000) - this.timerStartedAt);
				this.model.set({time: nt});
			} else {
				this.model.set({time: nt});
			}
			
			this.$el.find('.timer').text(this.timeToHuman(nt));
		},
		clearTimer: function() {
			if (this.timer != null) {
				window.clearInterval(this.timer);
				this.timer = null;
			}
		},
		timeToHuman: function(time) {
			var hours = parseInt( time / 3600 ) % 24,
				minutes = parseInt( time / 60 ) % 60,
				seconds = time % 60;
			
			return (hours > 0 ? hours + ":" : '') + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds  < 10 ? "0" + seconds : seconds);
		}
	});


	appCore.projectListItemView = appCore.baseView.extend({
		tagName:  "li",
		className: "project_list_item",
		events: {
		},
		initialize: function() {
			this.setTemplate('#project_list_item_block');
			
			this.listenTo(this.model, 'change:name', this.renderView);
			this.listenTo(this.model, 'destroy', this.destroy);
		}
	});


	appCore.homeView = appCore.baseView.extend({
		tagName:  "div",
		className: "home",
		homeList: null,
		events: {
		},
		initialize: function() {
			this.setTemplate('#home_block');
			
			this.homeList = new appCore.homeList();
			this.listenTo(this.homeList, 'add', this.addWorkunit);
			this.listenTo(this.homeList, 'sort', this.addAllWorkunit);
			this.homeList.fetch({remove: false});
		},
		addAllWorkunit: function() {
			this.removeChildViews();
			this.homeList.each(this.addWorkunit, this);
		},
		addWorkunit: function(model) {
			var view = new appCore.homeItemView({model: model});
			this.addChildView(view);
			this.$el.find('.workunitList').append(view.renderView().el);
		}
	});


	appCore.newView = appCore.baseView.extend({
		tagName:  "div",
		className: "new_view",
		events: {
			'submit #new_workunit': 'saveWorkunit',
			'click .manual': 'saveManual'
		},
		initialize: function() {
			this.setTemplate('#new_block');
		},
		saveManual: function(e) {
			e.preventDefault();
			this.handleSave('manual');
		},
		saveWorkunit: function(e) {
			e.preventDefault();
			this.handleSave('timer');
		},
		handleSave: function(type) {
			var new_description = this.$el.find('.new_description'),
				new_project = this.$el.find('.new_project'),
				model = new appCore.workunit({
					description : new_description.val() || 'untitled',
					project_id : new_project.val(),
					type: type,
					running: (type == 'timer' ? 1 : 0),
					time : 0
				});

			model.save([], {
				success: function(model, xhr, opt) {
					var project = window.time.projects.get(model.get('project_id'));
					if (project) {
						project.workunits.add(model);
						this.renderView();
					}
				}.bind(this)
			});
		}
	});


	appCore.homeItemView = appCore.workunitBase.extend({
		tagName:  "div",
		className: "home_item",
		events: {
			'click .toggle_timer': 'switchState',
		},
		initialize: function() {
			this.setTemplate('#home_item_block');
		},
		destroy: function() {
			this.clearTimer();
			this.removeChildViews();
			this.remove();
		},
		renderView: function() {
			this.render();
			this.toggleTimer();
			return this;
		},
		projectName: function() {
			return window.time.projects.get(this.model.get('project_id')).get('name');
		},
		switchState: function(e) {
			e.preventDefault();
			this.model.save({}, {
				url: (this.model.url() + (this.model.get('running') == 0 ? '/start' : '/pause')),
				success: function(model, xhr, options) {
					var project = window.time.projects.get(model.get('project_id'));
					if (typeof(project) != 'undefined' && project.workunits != null) {
						var workunit = project.workunits.get(model.get('id'));
						if (typeof(workunit) != 'undefined' && workunit != null) {
							workunit.set(xhr);
						}
					}
					window.location.hash = 'project/' + model.get('project_id');
				}.bind(this)
			});
		}
	});


	appCore.projectPageView = appCore.baseView.extend({
		totalTime: 0,
		tagName:  "div",
		className: "project_page_view",
		events: {},
		initialize: function() {
			this.setTemplate('#project_page_block');
			this.model.setWorkunits();
		},
		renderView: function() {
			this.render();
			
			var newView = new appCore.newView({model: this.model});
			this.addChildView(newView);
			this.$el.find('#new').html(newView.renderView().el);

			var workunitListView = new appCore.workunitListView({model: this.model});
			this.addChildView(workunitListView);
			this.$el.find('#workunitList').html(workunitListView.renderView().el);
			
			this.totalTicker = this.$el.find('#totalTicker');
			this.listenTo(this.model.workunits, 'add', this.setTotal);
			this.listenTo(this.model.workunits, 'remove', this.setTotal);
			this.listenTo(this.model.workunits, 'change:time', this.setTotal);
			
			return this;
		},
		setTotal: function() {
			this.totalTime = _.reduce(this.model.workunits.pluck('time'), function(memo, num){ return memo + parseInt(num); }, 0);
			this.totalTicker.text(this.timeToHuman(this.totalTime));
		},	
		timeToHuman: function(time) {
			var hours = parseInt( time / 3600 ) % 24;
			var minutes = parseInt( time / 60 ) % 60;
			var seconds = time % 60;
			
			return (hours > 0 ? hours + ":" : '') + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds  < 10 ? "0" + seconds : seconds);
		}
	});


	appCore.workunitListView = appCore.baseView.extend({
		tagName:  "div",
		className: "workunit_list",
		events: {
		},
		initialize: function() {
			this.setTemplate('#workunit_list_block');

			this.listenTo(this.model.workunits, 'add', this.addWorkunit);
			this.listenTo(this.model.workunits, 'sort', this.addAllWorkunit);
		},
		renderView: function() {
			this.render();
			this.addAllWorkunit();
			return this;
		},
		addAllWorkunit: function() {
			this.removeChildViews();
			this.model.workunits.each(this.addWorkunit, this);
		},
		addWorkunit: function(model) {
			var view = new appCore.workunitView({model: model});
			this.addChildView(view);
			this.$el.append(view.renderView().el);
		}
	});


	appCore.workunitView = appCore.workunitBase.extend({
		tagName:  "div",
		className: "workunit_view",
		editing: false,
		events: {
			'click .toggle_timer': 'switchTimer',
			'click .delete': 'deleteModel',
			'click .edit': 'toggleEdit',
			'blur .editing': 'saveEdit',
			'click .add_time': 'manualUpdate',
			'click .minus_time': 'manualUpdate'
		},
		initialize: function() {
			this.setTemplate('#workunit_block');
			this.listenTo(this.model, 'destroy', this.destroy);
		},
		renderView: function() {
			this.render();
			this.toggleTimer();
			return this;
		},
		toggleEdit: function(e) {
			e.preventDefault();
			
			this.editing = this.editing == true ? false : true ;
			this.renderView();
			
			if (this.editing == true) {
				this.$el.find('.edit_input').focus().val(this.model.get('description'));
			}
		},
		saveEdit: function(e) {
			this.model.save({description: e.currentTarget.value}, {success: function() {
				this.editing = false;
				this.renderView();
			}.bind(this)});
		},
		deleteModel: function(e) {
			e.preventDefault();
			
			this.model.destroy({success: function() {
				this.destroy();
			}.bind(this)});
		},
		destroy: function() {
			this.clearTimer();
			this.removeChildViews();
			this.remove();
		},
		switchTimer: function(e) {
			e.preventDefault();
			this.model.save({}, {
				url: (this.model.url() + (this.model.get('running') == 0 ? '/start' : '/pause')),
				success: function() {
					this.toggleTimer();
					this.model.collection.sort();
			}.bind(this)});
		},
		manualUpdate: function(e) {
			e.preventDefault();
			
			var increase_value = $(e.currentTarget).hasClass('add_time') ? 600 : -600 ,
				value = parseInt(this.model.get('time')) + increase_value;
			
			this.model.save({time: (value < 0 ? 0 : value)}, {url: this.model.url()+'/manual-update'});
			this.$el.find('.timer').text(this.timeToHuman(this.model.get('time')));
		}
	});


	appCore.notFoundView = appCore.baseView.extend({
		message: null,
		tagName:  "div",
		className: "not_found",
		events: {
		},
		initialize: function() {
			this.setTemplate('#not_found_bock');
		},
		setMessage: function(message) {
			if (message) {
				this.message = message;
			}
			return this;
		}
	});


});