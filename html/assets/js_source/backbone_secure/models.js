var appCore = appCore || {};

appCore.project = appCore.project || {} ;

appCore.home = appCore.home || {} ;
appCore.workunit = appCore.workunit || {} ;
appCore.entry = appCore.entry || {} ;


$(function () {
	'use strict';
	
	
	appCore.project = appCore.baseModel.extend({
		color: null,
		workunits: null,
		defaults: {
			id: null,
			name: null,
			owner_id: null,
			color: null,
			created_at: null,
			updated_at: null,
		},
		url: function() {
			return (this.get('id') != null) ? '/api/project/' + this.get('id') : '/api/project';
		},
		initialize: function() {
		},
		setWorkunits: function() {
			if (this.workunits == null) {
				this.workunits = new appCore.workunitList().setProject(this);
				this.workunits.doFetch();
			}
			return this.workunits;
		}
	});
	
	
	appCore.workunit = appCore.baseModel.extend({
		defaults : {
			id : null,
			project_id : null,
			creator_id : null,
			description : null,
			running : 0,
			type: 'timer',
			created_at : null,
			updated_at : null,
			time : 0,
			last_started: null
		},
		url : function() {
			return (this.get('id') != null) ? '/api/project/' + this.get('project_id') + '/workunit/' + this.get('id') : '/api/workunit';
		}
	});
	
	
	appCore.entry = appCore.baseModel.extend({
		defaults : {
			id : null,
			workunit_id : null,
			start_time : null,
			end_time : null
		},
		url : function() {
			return '/api/task/' + this.get('task_id') + '/details';
		}
	});
		
	
});