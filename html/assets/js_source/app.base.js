var appCore = appCore || {};

appCore.bSync = Backbone.sync;
appCore.fuel = appCore.fuel || {};
appCore.connections = appCore.connections || {};
appCore.baseModel = appCore.baseModel || {} ;
appCore.baseCollection = appCore.baseCollection || {} ;
appCore.baseView = appCore.baseView || {} ;
appCore.modalView = appCore.modalView || {} ;


$(function () {
	'use strict';
	
	
	Backbone.ajax = function(request) {
		
		if (typeof(request.dataType) == 'undefined') { request.dataType = 'json'; }
		
		var selfSuccess = request.success;
		var selfError = request.error;
		
		request.success = function(xhr, statusTxt, options) {
			if (typeof(xhr.response) != 'undefined' && typeof(xhr.response.messages) != 'undefined') {
				$.each(xhr.response.messages, function(error_type, errors) {
					if (error_type != 'validation_error') {
						$.each(errors, function(key, error) {
							new appCore.fuel.notify(	error_type + ' : ' + error	);
						});
					}
				});
			}
			
			if (typeof selfSuccess != 'undefined') {	// pass back the default success if set
				try { var data = xhr.response.data; } catch(err) { var data = []; }
				selfSuccess(data, statusTxt, options);
			}
		};
		
		request.error = function(xhr, statusTxt, options) {
			if (xhr.responseText != '') { var xhrResponse = jQuery.parseJSON(xhr.responseText); }
	  		new appCore.fuel.notify((typeof(xhrResponse) == 'object') ? xhrResponse.message : 'An unknow error occured. Please try again.');
	    	
	    	if (typeof selfError != 'undefined') {		// pass back the default error if set
	    		selfError(xhr, statusTxt, options);
	    	}
		};
		
		return Backbone.$.ajax.apply(Backbone.$, [request]);
	};
	
	
	appCore.fuel = {
		notify: function(message) {
			if ((typeof message == 'undefined') || (typeof message == 'object')) { message = 'An unknown error occured, Please try again.'; }
			alert(message);
		},
		utcDate: function() {
			return 0;
		},
		utcDateToUser: function(timestamp) {
			var date = new Date(timestamp * 1000);
			return new Date((timestamp * 1000) + appCore.fuel.dateOffset(date));
		},
		toUtcDate: function(date) {
			return ( new Date(date.getTime() - appCore.fuel.dateOffset(date)).getTime() ) / 1000;
		},
		dateOffset: function(date) {
			return (date.getTimezoneOffset() * 60) * 1000;	// in ms
		},
		pad: function(str, max) {
			return (str+'').length < max ? this.pad("0" + str, max) : str;
		}
	};
	
	
	appCore.connections = {
		currentConnections: 0,
		totalConnections: 0,
		bar: null,
		barInner: null,
		barHeight: 0,
		barWidth: 0,
		simulateTimer: null,
		simulateIncrease: 2,
		connectionBarWidth: 8,
		add: function() {
			this.currentConnections++;
			this.totalConnections++;
			this.simulateLoad(true);
			this.updateBar();
		},
		remove: function() {
			this.currentConnections = (this.currentConnections == 1) ? 0 : (this.currentConnections - 1) ;
			this.simulateLoad(false);
			this.updateBar();
		},
		updateBar: function() {
			if (this.bar == null) { this.generateBar(); }
			
			if (this.currentConnections == 0) {
				var _this = this;
				this.barHeight = 0;
				this.barWidth = 100;
				
				setTimeout(function() {
					if (_this.currentConnections == 0) {
						_this.barInner.css('width', '0%');
						_this.totalConnections = 0;
						_this.simulateIncrease = 2;
					}
				}, 500);
			} else {
				this.barHeight = 5,
				this.barWidth = ((this.connectionBarWidth * this.totalConnections) / this.currentConnections);
			}
			
			this.bar.css('height', this.barHeight + 'px');
			this.barInner.css('width', this.barWidth + '%');
		},
		simulateLoad: function(simulate) {
			if ((simulate == true) || (this.currentConnections > 0)) {
				var _this = this;
				this.simulateTimer = setInterval(function() {
					_this.barWidth += _this.simulateIncrease;
					_this.barInner.css('width', _this.barWidth + '%');
					
					if (_this.simulateIncrease > 0) {
						_this.simulateIncrease += -0.1;
					} else {
						_this.resetSimulate();
					}
				}, 250);
			} else {
				this.resetSimulate();
			}
		},
		resetSimulate: function() {
			if (this.simulateTimer != null) {
				clearInterval(this.simulateTimer);
				this.simulateTimer = null;
				this.simulateIncrease = 2;
			}
		},
		generateBar: function() {
			if (this.bar != null) { return; }
			this.bar = $('<div class="progress progress-striped active connection_bar"></div>');
			this.barInner = $('<div class="progress-bar"><span class="sr-only"></span></div>');
			
			this.barInner.appendTo(this.bar);
			this.bar.appendTo('#loading_block');
		}
	};
	
	
	appCore.baseModel = Backbone.Model.extend({
		
	});
	
	
	appCore.baseCollection = Backbone.Collection.extend({
		
		parse: function (resp) {
			return _.map(resp, function(v) { return v; });
		},
		
		initialize: function(models, options) {
			var $this = this;
			if (typeof(models) != 'undefined') { $this.add(models); }
			if (typeof(options) == 'object') { $.each(options, function(k,v) { $this[k] = v; }); }
		},
		
		set_option: function(key, value) {
			this[key] = value;
		}
		
	});
	
	
	appCore.baseView = Backbone.View.extend({
		addChildView: function(view) {
			if (typeof(this.childViews) == 'undefined') { this.childViews = []; }
			view.parentView = this;
			this.childViews.push(view);
		},
		removeChildViews: function() {
			var _this = this;
			if (typeof(this.childViews) != 'undefined') {
				$.each(this.childViews, function(k, view) {
					view.destroy();
					delete _this.childViews[k];
				});
				this.childViews = [];
			}
		},
		destroy: function() {
			this.removeChildViews();
			this.remove();
		},
		setTemplate: function(selector) {
			this.template = _.template($(selector).html());
		},
		renderView: function() {
			this.render();
			return this;	
		},
		render: function() {
			if (typeof(this.template) != 'undefined') {
				if (typeof(this.model) == 'object' && typeof(this.model.toJSON) != 'undefined') {
					this.$el.html(this.template(this.model.toJSON()));
				} else {
					this.$el.html(this.template());
				}
				return this;
			}
		},
		saveView: function(e) {
			e.preventDefault();
			this.handleSaveView();
		},
		handleSaveView: function(manual_data, manual_options) {
			var data = this.getViewData(manual_data),
				options = manual_options || {};
			
			this.model.save(data, options);
			
			this.$el.find('.messages_block').empty();
			this.$el.find('.validation_error').remove();
			this.$el.find('.action_btn').attr('disabled', 'disabled');
			this.$el.find('.submit').button('loading');
		},
		getViewData: function(manual_data) {
			var _this = this,
				data = manual_data || {};
			
			$.each(this.model.attributes, function(k, v) {
				var fel = _this.$el.find('[name='+k+']');
				if (fel.length > 0) {
					data[k] = fel.val();
				}
			});
			
			return data;
		},
		sync_validate: function(model, xhr, options) {
			var _this = this;
			this.$el.find('.action_btn').removeAttr('disabled');
			this.$el.find('.submit').button('reset');
			this.displayMessages(xhr);
			
			// Display validation messages
			if (typeof(xhr.response) != 'undefined' && typeof(xhr.response.messages) != 'undefined') {
				$.each(xhr.response.messages, function(error_type, errors) {
		    		if (error_type == 'validation_error') {
			    		$.each(errors, function(key, error) {
	    					_this.$el.find('[name='+key+']').after($('<span class="validation_error">'+error+'</span>'));
	    				});
		    		}
	    		});
	    	}
		},
		sync_error: function() {
			this.$el.find('.action_btn').removeAttr('disabled');
			this.$el.find('.submit').button('reset');
		},
		displayMessages: function(xhr) {
			if (typeof(xhr.response) != 'undefined' && typeof(xhr.response.messages) != 'undefined') {
				var messages_block = this.$el.find('.messages_block');
				
				if (messages_block.length > 0) {
			    	messages_block.empty();
					$.each(xhr.response.messages, function(error_type, errors) {
			    		if (error_type != 'validation_error') {
			    			var error_container = $('<div class="alert alert-' + error_type + '"></div>');
			    			$.each(errors, function(key, error) {
			    				error_container.append($('<p>' + error + '</p>'));
			    			});
			    			messages_block.append(error_container);	
			    		}
		    		});
				}	
			}
		},
		date: function(timestamp) {
			if (timestamp == null) { return null; }
			
			var date = new Date(timestamp * 1000),
				hours = date.getHours(),
				meridian = 'am';
				
			if (hours > 12) {
				hours = this.pad(hours-12, 2);
				meridian = 'pm';
			} else {
				hours = this.pad(hours, 2);
			}
			
			return date.getFullYear() + '-' + (date.getMonth()+1) + '-' + date.getDate() + ' ' + hours + ':' + this.pad(date.getMinutes(), 2) + ' ' + meridian;
		},
		dateToTimestamp: function(date) {
			return date.getTime() / 1000;
		},
		pad: function(str, max) {
			return (str+'').length < max ? this.pad("0" + str, max) : str;
		}
	});
	
	
	appCore.modalView = appCore.baseView.extend({
		events: {
			'click .close': 'removeModal',
			'click .modal-backdrop': 'removeModal',
		},
		show: function() {
			$(document.body).append(this.render().el);
		},
		removeModal: function(e) {
			e.preventDefault();
			this.remove();
		}
	});
	
	
});