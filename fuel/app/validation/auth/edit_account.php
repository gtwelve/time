<?php

return array(
	'first_name' => array(
		'label' => 'First Name',
		'rules' => array(
			array('type' => 'required'),
			array('type' => 'max_length', 'param' => '32')
		)
	),
	'last_name' => array(
		'label' => 'Last Name',
		'rules' => array(
			array('type' => 'required'),
			array('type' => 'max_length', 'param' => '32')
		)
	),
	'email' => array(
		'label' => 'Email',
		'rules' => array(
			array('type' => 'required'),
			array('type' => 'max_length', 'param' => '32'),
			array('type' => 'valid_email'),
	//		array('type' => 'custom', 'param' =>	function($email) {
	//													$user = \Model_User::forge();
	//													if(Auth::check()) {
	//														$user_obj = \Model_User::query()->where('id', \Arr::get(Auth::get_user_id(), 1))->get_one();
	//														if ($user_obj->email === $email) { return true; }
	//													}
	//													$exists = \DB::select(\DB::expr('COUNT(*) as total_count'))->from($user->table())->where('email', '=', $email)->execute()->get('total_count');
	//													Validation::active()->set_message('field_email', 'The email address :value is already in use by a user.');													
	//													return (bool) !$exists;
	//												})
		)
	)
);