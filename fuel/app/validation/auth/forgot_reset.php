<?php

return array(
	'password' => array(
		'label' => 'Password',
		'messages' => array(
			'match_pattern' => 'Your password must contain one upper case character and one number.'
		),
		'rules' => array(
			array('type' => 'required'),
//			array('type' => 'min_length', 'param' => '7'),
			array('type' => 'max_length', 'param' => '32'),
//			array('type' => 'match_pattern', 'param' => '/[A-Z]/'),
//			array('type' => 'match_pattern', 'param' => '/[a-z]/'),
//			array('type' => 'match_pattern', 'param' => '/[0-9]/')
		)
	),
	'repeat_password' => array(
		'label' => 'Repeat Password',
		'rules' => array(
			array('type' => 'required'),
			array('type' => 'match_field', 'param' => 'password')
		)
	),
);