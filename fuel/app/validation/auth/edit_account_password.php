<?php

return array(
	'old_password' => array(
		'label' => 'Current Password',
		'rules' => array(
			array('type' => 'required'),
			array('type' => 'custom', 'param' =>	function($password) {
														if(\Auth::check()) {
															$user_obj = Model_User::query()->where('id', \Arr::get(\Auth::get_user_id(), 1))->get_one();
															if ($user_obj->password === \Auth::instance()->hash_password($password)) { return true; }
														}
														\Validation::active()->set_message('field_current_password', 'You did not enter your current password correctly. Please try again.');
														return false;
													})
		)
	),
	'password' => array(
		'label' => 'New Password',
		'messages' => array(
			'match_pattern' => 'Your password must contain one upper case character and one number.'
		),
		'rules' => array(
			array('type' => 'required'),
//			array('type' => 'min_length', 'param' => '7'),
			array('type' => 'max_length', 'param' => '32'),
//			array('type' => 'match_pattern', 'param' => '/[A-Z]/'),
//			array('type' => 'match_pattern', 'param' => '/[a-z]/'),
//			array('type' => 'match_pattern', 'param' => '/[0-9]/')
		)
	),
	'repeat_password' => array(
		'label' => 'Repeat New Password',
		'rules' => array(
			array('type' => 'required'),
			array('type' => 'match_field', 'param' => 'password')
		)
	),
);