<?php

return array(
	'email' => array(
		'label' => 'Email',
		'rules' => array(
			array('type' => 'required'),
			array('type' => 'valid_email'),
			array('type' => 'max_length', 'param' => '255')
		)
	),
	'password' => array(
		'label' => 'Password',
		'rules' => array(
			array('type' => 'required'),
			array('type' => 'max_length', 'param' => '32')
		)
	)
);