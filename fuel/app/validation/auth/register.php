<?php

return array(
	'first_name' => array(
		'label' => 'First Name',
		'rules' => array(
			array('type' => 'required')
		)
	),
	'last_name' => array(
		'label' => 'Last Name',
		'rules' => array(
			array('type' => 'required')
		)
	),
	'email' => array(
		'label' => 'Email',
		'rules' => array(
			array('type' => 'required'),
			array('type' => 'valid_email'),
			//array('type' => 'custom', 'param' =>	function($email) {
			//											$user = \Model_User::forge();
			//											if ( ! $user->is_new() and $user->email === $email) { return true; }
			//											$exists = \DB::select(\DB::expr('COUNT(*) as total_count'))->from($user->table())->where('email', '=', $email)->execute()->get('total_count');
			//											Validation::active()->set_message('field_email', 'The email address :value is already in use by a user. You might already have an account.');													
			//											return (bool) !$exists;
			//										})
		)
	),
	'password' => array(
		'label' => 'Password',
		'messages' => array(
			'match_pattern' => 'Your password must contain one upper case character, one lower case character and one number.'
		),
		'rules' => array(
			array('type' => 'required'),
	//		array('type' => 'min_length', 'param' => '7'),
			array('type' => 'max_length', 'param' => '32'),
	//		array('type' => 'match_pattern', 'param' => '/[A-Z]/'),				array('type' => 'match_pattern', 'param' => '/[a-z]/'),
	//		array('type' => 'match_pattern', 'param' => '/[0-9]/')
		)
	)
);