<?php

return array(
	'project_id' => array(
		'label' => 'Project',
		'rules' => array(
			array('type' => 'required'),
		)
	),
	'description' => array(
		'label' => 'Description',
		'rules' => array(
			array('type' => 'required'),
		)
	),
	'type' => array(
		'label' => 'Type',
		'rules' => array(
			array('type' => 'required'),
		)
	),
);