<?php

return array(
	'name' => array(
		'label' => 'Name',
		'rules' => array(
			array('type' => 'required'),
		)
	)
);