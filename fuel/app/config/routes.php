<?php
return array(
	// Default routes
		'_root_' => array('public_default/index', 'name' => 'home'),
		'_404_' => 'public_default/404',
		'_403_' => 'public_default/403',
	
	// Auth
		'login'	=> array('public_auth/index', 'name' => 'login'),
		'logout' => array('public_auth/logout', 'name' => 'logout'),
	//	'register' => array('public_auth/register', 'name' => 'register'),
	//	'register/activate/(:num)/(:alnum)' => 'public_auth/activate/$1/$2',
	//	'forgot' => 'public_auth/forgot',
	//	'forgot/reset/(:num)/(:alnum)' => 'public_auth/forgot_reset/$1/$2',
	//	'resend/(:alnum)' => 'public_auth/resend/$1',
	
	
	// Testing
		
	
	
	// API routes
		// Auth
			'api/authenticate' => 'api_auth/authenticate',
		// Public
		// Secure
			'api/workunit' => 'api_workunit/workunit',
			
			'api/project/(:project_id)/workunit/(:workunit_id)/manual-update' => 'api_workunit/workunit_manual/$1/$2',
			'api/project/(:project_id)/workunit/(:workunit_id)/start' => 'api_workunit/workunit_start/$1/$2',
			'api/project/(:project_id)/workunit/(:workunit_id)/pause' => 'api_workunit/workunit_pause/$1/$2',
			'api/project/(:project_id)/workunit/(:workunit_id)' => 'api_workunit/workunit/$1/$2',
			'api/project/(:project_id)/workunit' => 'api_workunit/workunit/$1',
			
			'api/project/(:project_id)/new-color' => 'api_project/new_color/$1',
			'api/project/(:project_id)' => 'api_project/project/$1',
			'api/project' => 'api_project/project',
			
			'api/home' => 'api_home/home',
			
		// System
			'api/system' => 'api_api/system',
			'api/(:any)' => 'api_api/404',	// Make all other API routes show the 404 page
			'api' => 'api_api/index',
			
			
			
			
				
				
				
	// App routes
		// Public
			'styles' => 'public_default/styles',
			'gencolor' => 'public_default/gencolor',
		// Secure
	
	
		
		
	
	
	
		
		
	
	
	// All other routes are 404
		//'(:any)' => 'public_default/404',
	
);