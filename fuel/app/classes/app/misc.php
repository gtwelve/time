<?php


class App_Misc
{
	
	public static function check_date($date, $format)
	{
		if (is_null($format)) { return false; }
		
		try {
			Date::create_from_string($date, $format);
			return true;
		} catch (UnexpectedValueException $e) {
			return false;
		}
	}
	
	
	public static function format_date($timestamp, $format = 'local')
	{
		return Date::forge($timestamp)->format($format);
	}
	
	
	public static function print_r($data)
	{
		echo('<textarea style="width: 100%; height: 100%;">' . print_r($data, true) . '</textarea>');
		exit;
	}
	
	
	public static function create_color()
	{
		$hex_string = '#';
		$num_values = array();
		
		for ($i = 0; $i < 3; $i++) {
			$low = 40 + (40 * $i);
			$high = 250 - (25 * $i);
			$num_values[$i] = mt_rand($low, $high);
		}
		
		$min = min($num_values);
		$max = max($num_values);
		$diff = round(($max-$min) / 4);
			
		foreach ($num_values as $k => $value) {
			$v = ($value == $min) ? ($min+$diff) : (($value == $max) ? ($max-$diff) : $value) ;
			$hex_string .= str_pad(dechex($v), 2, '0', STR_PAD_LEFT);
		}
		
		return strtoupper($hex_string);
	}
	
	
}