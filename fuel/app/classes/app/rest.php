<?php


class App_Rest extends Controller_Rest
{
	public function before()
	{
		parent::before();
		
		$this->identity = Model_User::identity();
	}
	
	
	
	
	/**
	 * Formats a rest response with the request status and message
	 * This is used to determine if a request was successful of not
	 *
	 * @param	Object	$data	The data to of the request
	 * @return  Array
	 */
	public function forge($data = false)
	{
		if ($data == false) { $data = array(); }
		
		$response = array();
		$response['status'] = !isset($data['status']) ? 200 : $data['status'] ;
		$response['message'] = !isset($data['message']) ? 'Request OK' : $data['message'] ;
		unset($data['status'], $data['message']);
		$response['response'] = $data;
		
		$messages = App_Msg::get();
		if (!empty($messages)) { $response['response']['messages'] = $messages; }
		
		if (empty($response['response'])) { unset($response['response']); }
		
		return $this->response($response, $response['status']);
	}
	
	
	
	
	/**
	 * Converts a object to an array and prepares the data for json
	 */
	public function clean_data($data, $strip_keys = false)
	{
		$data = Format::forge($data)->to_array();
		$data = $this->clean_data_types($data);
		
		if ($strip_keys) {
			return array_values($data);
		} else {
			return $data;
		}
		
	}
	
	
	
	
	/**
	 * Alias for the static get_load_param function in App_Base
	 */
	public function load_param($param, $model, $load_function = 'load_by_id')
	{
		return App_Base::get_load_param($param, $model, $load_function);
	}
	
	
	
	
	/**
	 * Alias for the static require_clearance function in App_Base
	 */
	public function set_clearance($clearance)
	{
		return App_Base::require_clearance($clearance);
	}
	
	
	
	
	/**
	 * Converts strings to ints and floats if the value is numeric
	 */
	private function clean_data_types(&$data)
	{
		if (is_array($data)) {
			foreach ($data as $k => $d) {
				if (is_array($d)) {
					$data[$k] = $this->clean_data_types($d);
				} else {
					if (is_numeric($d)) {
						$data[$k] = (is_float($d)) ? (float) $d : (int) $d ;
					}
				}
			}
		}
		
		return $data;
	}
	
	
	
}