<?php

class App_Val 
{
	public static $validation = NULL;
	public static $dynamic_validation = NULL;
	public static $require_csrf = false;
	public static $rule_prefix = 'field_';
	public static $rule_message_prefix = 'custom_field_message_';
	public static $data = array();
	
	/**
	 * Builds and runs a validation
	 *
	 * @param	string	$validation_name		The validation file with the validation parameters
	 * @return	object	Bool
	 */
	public static function forge($validation_name = NULL, $data = NULL)
    {
    	// Get the data
    	if (is_string($data)) {
	    	self::$data = Input::$data();
    	} elseif (is_array($data)) {
    		self::$data = $data;
    	} elseif ((Request::active()->controller_instance instanceof App_Rest) || (Request::main()->controller_instance instanceof App_Rest)) {
    		// when Backbone.sync sends up a request to save a model, its attributes will be passed, serialized as JSON, and sent in the HTTP body with content-type application/json
    		$rest_data = Input::json();
    		if (!empty($rest_data)) {
    			self::$data = $rest_data;
    		} else {
	    		self::$data = Input::param();
    		}
    	} else {
	    	self::$data = Input::param();
    	}
    	
    	$val = self::build($validation_name);
    	
    	$run = $val->run(self::$data);
    	if ($run) {
    		return $val;
    	} else {
	    	foreach($val->error() as $k => $v) {
				$custom_message = $val->get_message(self::$rule_message_prefix.$v->rule.'_'.$k);
				App_Msg::add_validation_error($k, ($custom_message != false) ? $custom_message : $v->get_message());
			}
			return false;
    	}
    }
	
	
	
	
	/**
	 * Forges a new Validation object with dynamic javascript if specified
	 *
	 * @param	string	$validation_name		The validation file with the validation parameters
	 * @return	object	A new validation object
	 */
	public static function build($validation_name = NULL)
    {
		if ($validation_name == NULL) throw new Exception("No validation was called.");	
		
		$validation_rules = Finder::search('validation/', $validation_name);
		
		if ($validation_rules == false) {
			throw new Exception("No validation method for '" . $validation_name . "' could be found.");	
			
		} else {
			$rules = include($validation_rules);
			$unique_validation = Str::random('unique');
			$val = Validation::forge($unique_validation);
			
			if (!is_array($rules)) throw new Exception("The validation method is invalid.");
			
			foreach($rules as $rule_key => $rule) {
				if ($rule_key == 'require_csrf') {
					self::$require_csrf = $rule;
				} else {
					if (!isset($rule['label'])) throw new Exception("No label set for validation rule.");
					if (!isset($rule['rules'])) throw new Exception("No rules set for validation rule.");
					
					$val_field = $val->add($rule_key, $rule['label']);
					
					foreach($rule['rules'] as $rule_part) {
						if (!isset($rule_part['type'])) throw new Exception("No type set for validation rule.");
						
						if ($rule_part['type'] == 'custom') {
							if (!isset($rule_part['param']) || !is_callable($rule_part['param'])) throw new Exception("No function passed to custom validation rule.");
							$val_field->add_rule(array(self::$rule_prefix.$rule_key => $rule_part['param']));
							
						} elseif ($rule_part['type'] == 'valid_id') {
							if (!isset($rule_part['param']) || !is_string($rule_part['param'])) throw new Exception("No model name passed to validate ID.");
							if (!class_exists($rule_part['param'])) throw new Exception("An invalid model name has been used.");
							if (!method_exists($rule_part['param'], 'load_by_id')) throw new Exception("The specified model does not contain a 'load_by_id' function.");
							
							if (array_key_exists($rule_key, self::$data)) {
								$model_id = $rule_part['param']::load_by_id(self::$data[$rule_key]);
								if (is_null($model_id)) {
									$val_field->add_rule('valid_string', array('alpha'));
									$val->set_message(self::$rule_message_prefix.'valid_string'.'_'.$rule_key, 'The value you have given for ' . $rule['label'] . ' is invalid.');
								}
							}
						} elseif ($rule_part['type'] == 'valid_option') {	
							if (!isset($rule_part['param']) || !is_array($rule_part['param'])) throw new Exception("No options to validate aganist.");
							
							if (array_key_exists($rule_key, self::$data)) {
								$option_param = self::$data[$rule_key];
								$in_array = in_array($option_param, $rule_part['param'], true);
								
								if ($in_array == false) {
									self::$data[$rule_key] = '123';
									$val_field->add_rule('match_value', 'abc');
									$val->set_message(self::$rule_message_prefix.'match_value'.'_'.$rule_key, 'The option you have entered for ' . $rule['label'] . ' is invalid. Please enter one of the following: ' . implode(', ', $rule_part['param']) . '.');
								}
							}
						} else {
							if (isset($rule_part['param'])) {
								$val_field->add_rule($rule_part['type'], $rule_part['param']);
							} else {
								$val_field->add_rule($rule_part['type']);
							}
						}
					}
					
					if (isset($rule['messages'])) {
						foreach($rule['messages'] as $message_for => $message) {
							$val->set_message(self::$rule_message_prefix.$message_for.'_'.$rule_key, $message);
						}
					}
				}
			}
			
			// Check csrf token
			if (self::$require_csrf == true) {
				$rule_key = \Config::get('security.csrf_token_key');
				$val->set_message(self::$rule_prefix.$rule_key, 'Your request cannot be carried out. The request did not contain valid security headers. Please try again. If this problem persists try another browser.');
				$csrf_field = $val->add($rule_key, 'CSRF Token');
				$csrf_field->add_rule(array(self::$rule_prefix.$rule_key => function($token) {
				    return \Security::check_token();
				}));
			}
			
			return self::$validation = $val;
		}
	}
	
	
	
	
	/**
	 * Converts a FuelPHP validation object into a simple array so
	 * ajax can understand it
	 *
	 * @param	Object	$val		The fuelPHP validation object to convert
	 * @return	Array
	 */
	public static function get_errors($val)
	{
		$output = array();
		
		foreach($val->error() as $k => $v) {
			$custom_message = $val->get_message(self::$rule_message_prefix.$v->rule.'_'.$k);
			$output[$k] = ($custom_message != false) ? $custom_message : $v->get_message() ;
		}
		
		return $output;
	}
	
	
}