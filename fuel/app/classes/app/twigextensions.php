<?php
/**
 * @package    Twig
 * @version    1.0
 * @author     allfields
 * @link       http://allfields.co.nz
 */

/**
 * Provides Twig support for commonly used classes and methods.
 */
class App_TwigExtensions extends Twig_Extension
{
	/**
	 * Gets the name of the extension.
	 *
	 * @return  string
	 */
	public function getName()
	{
		return 'custom';
	}
	
	/**
	 * Sets up all of the functions this extension makes available.
	 *
	 * @return  array
	 */
	public function getFunctions()
	{
		return array(
			'implode'	=> new Twig_Function_Function('implode'),
			'config_get'	=> new Twig_Function_Function('Config::get'),
			'forge_view'	=> new Twig_Function_Function('View::forge'),
			'floor'	=> new Twig_Function_Function('floor'),
			'format_date'	=> new Twig_Function_Function('App_Misc::format_date'),
			'date_format_example'	=> new Twig_Function_Function('App_Misc::date_format_example'),
			'input_param'	=> new Twig_Function_Function('Input::param'),
			'query_clearance'	=> new Twig_Function_Function('App_Secure::query_clearance'),
			'time_ago' => new Twig_Function_Method($this, 'time_ago'),
			'truncate' => new Twig_Function_Function('Str::truncate'),
			'var_dump' => new Twig_Function_Function('var_dump'),
			'print_r' => new Twig_Function_Function('print_r'),
			'nl2br' => new Twig_Function_Function('nl2br'),
			'die' => new Twig_Function_Function('die'),
			'input_param' => new Twig_Function_Function('Input::param'),
			'fuel_timestamp'	=> new Twig_Function_Method($this, 'fuel_timestamp'),
			'get_timestamp'	=> new Twig_Function_Method($this, 'get_timestamp'),
			'intval'	=> new Twig_Function_Function('intval'),
			'instanceof'	=> new Twig_Function_Method($this, 'instance_of'),
			'fetch_csrf_token'	=> new Twig_Function_Function('Security::fetch_token'),
			'js_fetch_token'	=> new Twig_Function_Function('Security::js_fetch_token'),
			'js_set_token'	=> new Twig_Function_Function('Security::js_set_token'),
			'this_url'	=> new Twig_Function_Function('Uri::string'),
			'enviroment'	=> new Twig_Function_Method($this, 'enviroment'),
			'urlencode'	=> new Twig_Function_Function('urlencode'),
		);
	}
	
	
	
	/**
	 * Provides the url() functionality.  Generates a full url (including
	 * domain and index.php).
	 *
	 * @param   string  URI to make a full URL for (or name of a named route)
	 * @param   array   Array of named params for named routes
	 * @return  string
	 */
	public function url($uri = '', $named_params = array())
	{
		if ($named_uri = \Router::get($uri, $named_params))
		{
			$uri = $named_uri;
		}

		return \Uri::create($uri);
	}
	
	
	/**
	 * Creates and returns a fuel timestamp
	 *
	 * @return  int
	 */
	public function fuel_timestamp()
	{
		return \Date::forge()->get_timestamp();
	}
	
	
	/**
	 * Returns the timestamp from a date string
	 *
	 * @param	$date_obj	a Fuel date object
	 * @return  string
	 */
	public function get_timestamp($time, $format = 'mysql')
	{
		return \Date::create_from_string($time, $format)->get_timestamp();
	}
	
	
	/**
	 * Calculates the time since or until a timestamp.
	 *
	 * @param	$timestamp	The timestamp to calculate from
	 * @return  string
	 */
	public function time_ago($timestamp, $short_peroids = false)
	{
		return \App_Misc::time_ago($timestamp, NULL, $short_peroids);
	}
	
	
	/**
	 * Checks if a object is an instance of something
	 *
	 * @param	string	$object				The object to check
	 * @param	string	$instance_name		The instance name to check against
	 * @return  bool
	 */
	public function instance_of($object, $instance_name)
	{
		return ($object instanceof $instance_name) ? true : false ;
	}
	
	
	/**
	 * Returns the current enviroment
	 *
	 * @return  string
	 */
	public function enviroment()
	{
		return \Fuel::$env;
	}
	
}
