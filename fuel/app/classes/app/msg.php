<?php


class App_Msg
{
	public static $messages = array();
	public static $filter = true;
	
	
	public static function add($message, $type = 'error', $filter = true)
	{
		if (is_array($message)) {
			foreach ($message as $m) {
				self::$messages[$type][] = $m;
			}
		} else {
			self::$messages[$type][] = $message;
		}
		
		self::$filter = (($filter == false) || (self::$filter == false)) ? false : true ;
		
		View::set_global('messages', self::$messages, self::$filter);
	}
	
	
	public static function add_validation_error($field, $message)
	{
		self::$messages['validation_error'][$field] = $message;
		
		View::set_global('messages', self::$messages, self::$filter);
	}
	
	
	public static function get_type($type)
	{
		return isset(self::$messages[$type]) ? self::$messages[$type] : array() ;
	}
	
	
	public static function get()
	{
		return self::$messages;
	}
	
	
}