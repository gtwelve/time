<?php


class App_Base extends Controller
{
	public function before()
	{
		parent::before();
		
		$this->identity = Model_User::identity();
	}
	
	
	
	
	
	
	/**
	 * Forges a response from a controller method.
	 *
	 * @param	Object	$response	Usually a view object
	 * @return  Object
	 */
	public function forge($response)
	{
		$rr = Response::forge($response);
		$rr->set_header('Cache-Control', 'no-cache, no-store, max-age=0, must-revalidate');
		$rr->set_header('Expires', \Date::forge()->format('%a, %d %b %Y %T %Z'));
		$rr->set_header('Pragma', 'no-cache');
		
		return $rr;
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * Alias for the get_load_param function
	 */
	public function load_param($param, $model, $load_function = 'load_by_id')
	{
		return self::get_load_param($param, $model, $load_function);
	}
	
	
	/**
	 * Loads a named route param from the DB and returns it
	 *
	 * @param	String	$param	The named route param to load
	 * @param	String	$model	The named of the model the param uses
	 * @return  object/NULL
	 */
	public static function get_load_param($param, $model, $load_function = 'load_by_id')
	{
		$param_value = Request::active()->param($param);
		if (is_null($param_value)) { $param_value = Request::main()->param($param); }
		
		if (!is_null($param_value)) {
			$model_obj = $model::$load_function($param_value);
			if (is_null($model_obj)) { throw new HttpNotFoundException; }
			return $model_obj;
		} else {
			return NULL;
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * An alias for require_clearance function
	 *
	 * @return	NULL
	 */
	public function set_clearance($clearance)
	{
		return self::require_clearance($clearance);
	}
	
	
	/**
	 * Requires a user to have a specified 'clearence' level
	 * If they dont have the required clearence they are sent
	 * to the login page.
	 * 
	 * @param	string	$needed_clearance	The name of the user group needed to view the page
	 * @return	NULL
	 */
	public static function require_clearance($needed_clearance = NULL) {
		if (!self::clearance($needed_clearance)) {
			if (Input::is_ajax() || Request::active()->controller_instance instanceof App_Rest) {
				throw new App_Exception_HttpForbiddenException();
			} else {
				if (Model_User::identity() == NULL) {
					Response::redirect(Router::get('login') . '?after=' . Uri::string());
				} else {
					throw new App_Exception_HttpForbiddenException();
				}
			}
		}
	}
	
	
	/**
	 * A public method for checking a specified 'clearence'.
	 * 
	 * @param	string	$needed_clearance	The name of the user group needed to view the page
	 * @return	BOOL
	 */
	public static function query_clearance($needed_clearance, $identity = NULL) {
		return self::clearance($needed_clearance, $identity);
	}
	
	
	/**
	 * Checks if a user has a specified 'clearence'.
	 * 
	 * @param	string	$needed_clearance	The name of the user group needed to view the page
	 * @return	Bool
	 */
	private static function clearance($needed_clearance, $identity = NULL) {
		$base_identity = (!is_null($identity) && ($identity instanceof Model_User)) ? $identity : Model_User::identity() ;
		if ($base_identity != NULL) {
			$groups = \Config::get('simpleauth.groups');
			$user_group = $groups[$base_identity->group];
			return in_array($needed_clearance, $user_group['roles']) ? true : false ;
		} else {
			return false;
		}
	}
	
	
	
	
}