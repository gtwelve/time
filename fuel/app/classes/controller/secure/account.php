<?php
/**
 * Fuel is a fast, lightweight, community driven PHP5 framework.
 *
 * @package    Fuel
 * @version    1.6
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2013 Fuel Development Team
 * @link       http://fuelphp.com
 */

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 *
 * @package  app
 * @extends  Controller
 */
class Controller_Secure_Account extends App_Base
{
	public function before()
	{
		parent::before();
		
	}
	
	
	
	public function action_index()
	{
		$view = View::forge('secure/account/index.twig');
		
		if (Input::method() == 'POST') {
			Model_User::edit_account();
		}
		
		return Response::forge($view);
	}
	
	
	
	
	public function action_password()
	{
		$view = View::forge('secure/account/password.twig');
		
		if (Input::method() == 'POST') {
			Model_User::edit_password();
		}
		
		return Response::forge($view);
	}
	
	
}