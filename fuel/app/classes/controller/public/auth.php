<?php
/**
 * Fuel is a fast, lightweight, community driven PHP5 framework.
 *
 * @package    Fuel
 * @version    1.6
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2013 Fuel Development Team
 * @link       http://fuelphp.com
 */

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 *
 * @package  app
 * @extends  Controller
 */
class Controller_Public_Auth extends App_Base
{
	public function before()
	{
		parent::before();
		
		$this->after_authentication = '/' . Input::get('after', '');
		$this->after_logout = '/?logged=out';
		$this->after_registration = '/?registered=true';
		
		$this->require_activation = true;
		
		if (!is_null($this->identity) && (Uri::current() != Router::get('logout'))) {
			Response::redirect($this->after_authentication);
		}
	}
	
	
	
	
	
	
	public function action_index()
	{
		$view = View::forge('auth/login.twig');
		
		if (Input::method() == 'POST') {
			$login = Model_User::login();
			if ($login) {
				Response::redirect($this->after_authentication);
			}
		}
		
		return Response::forge($view);
	}
	
	
	
	
	
	
	
	
	
	public function action_logout()
	{
		Model_User::logout();
		Response::redirect($this->after_logout);
	}
	
	
	
	
	
	
	
	public function action_register()
	{
		$view = View::forge('auth/register.twig');
		
		if (Input::method() == 'POST') {
			$register = Model_User::register_user(100, $this->require_activation);
			if ($register) {
				if ($this->require_activation) {
					$view->registered = true;
				} else {
					Response::redirect($this->after_registration);
				}
			}
		}
		
		return Response::forge($view);
	}
	
	
	
	
	
	
	
	
	public function action_activate($user_id, $activation_code)
	{
		$view = View::forge('auth/activate.twig');
		
		$activation = Model_User::activate_user($user_id, $activation_code);
		if ($activation) {
			Response::redirect($this->after_authentication);
		}
		
		return Response::forge($view);
	}
	
	
	
	
	
	
	
	public function action_forgot()
	{
		$view = View::forge('auth/forgot.twig');
		
		if (Input::method() == 'POST') {
			$forgot = Model_User::password_forgot();
			if ($forgot) {
				$view->reset = true;
			}
		}
		
		return Response::forge($view);
	}
	
	
	
	
	
	public function action_forgot_reset($user_id, $reset_code)
	{
		$view = View::forge('auth/forgot_reset.twig');
		
		if (Input::method() == 'POST') {
			$reset = Model_User::password_reset($user_id, $reset_code);
			if ($reset === true) {
				Response::redirect($this->after_authentication);
			}
		} else {
			Model_User::check_password_reset_link($user_id, $reset_code);
		}
		
		return Response::forge($view);
	}
	
	
	
	
	
	public function action_resend($resend_code)
	{
		$view = View::forge('auth/resend.twig');
		
		$view->resent = Model_User::resend_email($resend_code);
		
		return Response::forge($view);
	}
	
	
	
	
}