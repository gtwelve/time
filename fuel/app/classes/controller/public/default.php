<?php
/**
 * Fuel is a fast, lightweight, community driven PHP5 framework.
 *
 * @package    Fuel
 * @version    1.6
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2013 Fuel Development Team
 * @link       http://fuelphp.com
 */

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 *
 * @package  app
 * @extends  Controller
 */
class Controller_Public_Default extends App_Base
{
	public function before()
	{
		parent::before();
	}
	
	
	
	/**
	 * The basic welcome message
	 *
	 * @access  public and secure
	 * @return  Response
	 */
	public function action_index()
	{
		if ($this->identity == null) {
			Response::redirect('/login');
		}
		
		$view = View::forge('secure/index.twig');
		
		return $this->forge($view);
	}
	

	
	
	/**
	 * Renders the default 404 page.
	 *
	 * @input   GET/POST
	 * @return  View/Rest
	 */
	public function action_404()
	{
		$view = View::forge('common/404.twig', array('title' => '404 Not Found'));
		
		return $this->forge($view);
	}
	
	
	
	
	/**
	 * Renders the default 403 (forbidden) page.
	 *
	 * @input   GET/POST
	 * @return  View/Rest
	 */
	public function action_403()
	{
		$view = View::forge('common/403.twig', array('title' => '403 Forbidden'));
		
		return $this->forge($view);
	}
	
	
	
	
	public function action_styles()
	{
		$color = urldecode(Input::get('color'));
		$styles = '';
		
		
		$styles .= '#header_block { background-color: ' . $color . '; }';
		$styles .= '.home_item .description, .home_item .description a { color: ' . $color . '; }';
		$styles .= '.workunit_view .title { color: ' . $color . '; }';
		
		
		print_r($styles);
		die();
	}
	
	
	
	public function action_gencolor()
	{
		$color = App_Misc::create_color();
		
		echo('<div style="width: 100%; height: 100%; background-color: ' . $color . ';">&nbsp;</div>');
		die();
	}
	
	
	
}
