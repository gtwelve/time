<?php
/**
 * Fuel is a fast, lightweight, community driven PHP5 framework.
 *
 * @package    Fuel
 * @version    1.6
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2013 Fuel Development Team
 * @link       http://fuelphp.com
 */

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 *
 * @package  app
 * @extends  Controller
 */
class Controller_Api_Api extends App_Rest
{
	public function before()
	{
		parent::before();
	}
	
	
	public function action_index()
	{
		throw new HttpNotFoundException;
	}
	
	
	public function action_404()
	{
		return $this->forge(array('status' => 404, 'message' => '404: The page you requested cannot be found.'));
	}
	
	
	public function action_403()
	{
		return $this->forge(array('status' => 403, 'message' => '403: You do not have permission to carry out this action.'));
	}
	
	
	public function action_system()
	{
		$this->set_clearance('admin');
		
		return $this->forge(array('The server time is ' . Date::forge()->format('system_pretty')));
	}
	
	
}