<?php
/**
 * The Opts (options) Controller.
 *
 * Loads options from the DB
 *
 * @package  app
 * @extends  App_Rest
 */
class Controller_Api_Workunit extends App_Rest
{
	
	public function before()
	{
		parent::before();
		
		$this->set_clearance('user');
		
		$this->project = $this->load_param('project_id', 'Model_Project');
		if (!is_null($this->project) && $this->project->owner_id != $this->identity->id) { throw new App_Exception_HttpForbiddenException(); }
		
		$this->workunit = $this->load_param('workunit_id', 'Model_Workunit');
		if (!is_null($this->workunit) && $this->workunit->creator_id != $this->identity->id) { throw new App_Exception_HttpForbiddenException(); }
	}
	
	
	public function get_workunit($project_id)
	{
		return $this->forge(array(
			'data' => $this->project->getWorkunits()
		));
	}
	
	
	public function post_workunit()
	{
		return $this->forge(array(
			'data' => Model_Workunit::add()
		));
	}
	
	
	public function put_workunit()
	{
		return $this->forge(array(
			'data' => Model_Workunit::edit($this->workunit)
		));
	}
	
	
	public function delete_workunit()
	{
		return $this->forge(array(
			'data' => $this->workunit->destroy()
		));
	}
	
	
	public function put_workunit_start()
	{
		return $this->forge(array(
			'data' => $this->workunit->start()
		));
	}
	
	
	public function put_workunit_pause()
	{
		return $this->forge(array(
			'data' => $this->workunit->pause()
		));
	}
	
	
	public function put_workunit_manual()
	{
		return $this->forge(array(
			'data' => $this->workunit->manual_time()
		));
	}
	
}