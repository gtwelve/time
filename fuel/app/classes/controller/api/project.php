<?php
/**
 * The Opts (options) Controller.
 *
 * Loads options from the DB
 *
 * @package  app
 * @extends  App_Rest
 */
class Controller_Api_Project extends App_Rest
{
	public function before()
	{
		parent::before();
		
		$this->set_clearance('user');
		
		$this->project = $this->load_param('project_id', 'Model_Project');
		if (!is_null($this->project) && $this->project->owner_id != $this->identity->id) { throw new App_Exception_HttpForbiddenException(); }
	}
	
	
	public function get_project()
	{
		return $this->forge(array(
			'data' => $this->identity->get_projects()
		));
	}
	
	
	public function post_project()
	{
		return $this->forge(array(
			'data' => Model_Project::add()
		));
	}
	
	
	public function put_project()
	{
		return $this->forge(array(
			'data' => Model_Project::edit($this->project)
		));
	}
	
	
	public function delete_project() 
	{
		return $this->forge(array(
			'data' => $this->project->destroy()
		));
	}
	
	
	public function put_new_color()
	{
		return $this->forge(array(
			'data' => $this->project->new_color()
		));
	}
	
	
}