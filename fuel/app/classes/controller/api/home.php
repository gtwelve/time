<?php
/**
 * The Opts (options) Controller.
 *
 * Loads options from the DB
 *
 * @package  app
 * @extends  App_Rest
 */
class Controller_Api_Home extends App_Rest
{
	public function before()
	{
		parent::before();
		
		$this->set_clearance('user');
	}
	
	
	public function get_home()
	{
		$data = $this->last_updated();
		
		return $this->forge(array(
			'data' => $data
		));
	}
	
	
	private function last_updated()
	{
		// Recently updated
		$limit = 10;
		$ru_q = DB::query('SELECT entry.workunit_id FROM entry LEFT JOIN workunit ON workunit.id=entry.workunit_id WHERE workunit.creator_id='.$this->identity->id.' GROUP BY entry.workunit_id ORDER BY MAX(entry.start_time) DESC LIMIT ' . $limit)->execute()->as_array('workunit_id');
		if (empty($ru_q)) { return array(); } // bail out of no results
		
		// Get the workunits
		$ts = Date::forge()->get_timestamp();
		$qs = '(SELECT SUM(IFNULL(entry.end_time,'.$ts.') - entry.start_time) FROM entry WHERE entry.workunit_id = workunit.id)';
		$st = '(SELECT MAX(entry.start_time) FROM entry WHERE entry.workunit_id = workunit.id)';
		$wq = DB::query('SELECT workunit.*, '.$qs.' AS time, '.$st.' AS last_started FROM workunit WHERE workunit.id IN ('.implode(',', array_keys($ru_q)).')')->execute()->as_array('id');
		
		return $wq;
	}
	
	
}