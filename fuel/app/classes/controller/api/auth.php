<?php
/**
 * The API Auth Controller.
 *
 * Handles authentication/and account calls made via the API
 *
 * @package  app
 * @extends  App_Rest
 */
class Controller_Api_Auth extends App_Rest
{
	public function before()
	{
		parent::before();
	}
	
	
	
	public function post_authenticate()
	{
		$login = Model_User::login();
		
		return $this->forge(array(
			'data' => array('success' => $login)
		));
	}
	
	
	public function action_authenticate()
	{
		throw new HttpNotFoundException;
	}
	
	
	
}