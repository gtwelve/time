<?php

class Model_Workunit extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'project_id',
		'creator_id',
		'description',
		'running',
		'type',
		'created_at',
		'updated_at'
	);
	protected static $_table_name = 'workunit';
	protected static $_primary_key = array('id');
	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => false
		)
	);
	protected static $_has_many = array(
	    'entries' => array(
	        'key_from' => 'project_id',
	        'model_to' => 'Model_Entry',
	        'key_to' => 'id',
	        'cascade_save' => false,
	        'cascade_delete' => false,
	    )
	);
	protected static $_belongs_to = array(
	    'workunit' => array(
	        'key_from' => 'project_id',
	        'model_to' => 'Model_Project',
	        'key_to' => 'id',
	        'cascade_save' => false,
	        'cascade_delete' => false,
	    )
	);
	
	
	/* Loads a workunit by ID
	 *
	 * @param	$workunit_id	int		The ID of the workunit to load
	 * @return	Object
	 */
	public static function load_by_id($workunit_id)
	{
		return self::query()->where('id', $workunit_id)->get_one();
	}
	
	
	/*
	 * Adds new workunit to the DB and creates a new entry (delegates to static::handle_save for the grunt work)
	 *
	 * @return	Object
	 */
	public static function add()
	{
		$workunit = self::forge();
		$workunit->creator_id = Model_User::identity_id();
		$workunit->running = 1;
		$workunit = static::handle_save($workunit);
		
		if ($workunit) {
			$workunit->create_entry($workunit->type == 'manual' ? true : false);
			$workunit->time = $workunit->get_time();
			$workunit->last_started = $workunit->last_started();
		}
		
		return $workunit;
	}
	
	
	/*
	 * Edits a workunit (delegates to static::handle_save for the grunt work)
	 *
	 * @param	$workunit		object		An instance of the workunit to save
	 * @return	Object
	 */
	public static function edit($workunit)
	{
		$workunit = static::handle_save($workunit);
		
		if ($workunit) {
			$workunit->time = $workunit->get_time();
		}
		
		return $workunit;
	}
	
	
	/*
	 * Handles validation and saving of workunits
	 *
	 * @param	$workunit		object		An instance of the workunit to save
	 * @return	Object
	 */
	private static function handle_save($workunit)
	{
		$val = App_Val::forge('workunit/workunit');
		if ($val) {
			if ($workunit->is_new()) {
				$workunit->type = $val->validated('type');
			}
			
			$workunit->project_id = $val->validated('project_id');
			$workunit->description = $val->validated('description');
			$workunit->save();
			
			return $workunit;
		}
		
		return false;
	}
	
	
	/*
	 * Destroys a workunit and any entries that belong to it
	 *
	 * @return	Bool
	 */
	public function destroy()
	{
		DB::delete('entry')->where('workunit_id', $this->id)->execute();
		return $this->delete();
	}
	
	
	/*
	 * Creates a workunit entry
	 *
	 * @param	$end	bool	Weather or not to create and end time the same as the start time, used to creating manual entries
	 * @return	Bool
	 */
	public function create_entry($end = false)
	{
		return Model_Entry::add($this->id, $end);
	}
	
	
	/*
	 * Gets the time for all entries of a workunit
	 *
	 * @return	int
	 */
	public function get_time()
	{
		$ts = Date::forge()->get_timestamp();
		$q = DB::query('SELECT SUM(IFNULL(entry.end_time,'.$ts.') - entry.start_time) AS time FROM entry WHERE entry.workunit_id = '.$this->id)->execute()->current();
		
		return is_numeric($q['time']) ? intval($q['time']) : 0 ;
	}
	
	
	/*
	 * Gets the last entry for a workunit
	 *
	 * @return	object
	 */
	public function last_entry()
	{
		return Model_Entry::query()->where('workunit_id', $this->id)->order_by('start_time', 'DESC')->get_one();
	}
	
	
	/*
	 * Sets the start time for the last entry if there is one
	 *
	 * @return	Bool
	 */
	public function last_started()
	{
		$last_entry = $this->last_entry();
		return (is_null($last_entry) ? 0 : $last_entry->start_time);
	}
	
	
	/*
	 * Starts a workunit
	 *
	 * @return	array
	 */
	public function start()
	{
		if ($this->running == 0) {
			$this->running = 1;
			$this->save();
			
			$entry = Model_Entry::add($this->id);
			$last_started = $entry->start_time;
		} else {
			$last_started = $this->last_started();
		}
		
		return array('time' => $this->get_time(), 'running' => $this->running, 'last_started' => $last_started);
	}
	
	
	/*
	 * Ends a workunit
	 *
	 * @return	array
	 */
	public function pause()
	{
		if ($this->running == 1) {
			$this->running = 0;
			$this->save();
			
			$entry = Model_Entry::end($this->id);
			$last_started = $entry->start_time;
		} else {
			$last_started = $this->last_started();
		}
		
		return array('time' => $this->get_time(), 'running' => $this->running, 'last_started' => $last_started);
	}
	
	
	/*
	 * Creates manual workunit
	 *
	 * @return	array
	 */
	public function manual_time()
	{
		$val = App_Val::forge('workunit/manual_time');
		if ($val) {
			$last = $this->last_entry();
			
			$last->end_time = $last->start_time + intval($val->validated('time'));
			$last->save();
			
			return array('time' => $this->get_time(), 'last_started' => $last->start_time);
		}
		
		return false;
	}
	
	
}