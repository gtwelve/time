<?php

class Model_Project extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'name',
		'owner_id',
		'color',
		'created_at',
		'updated_at'
	);
	protected static $_table_name = 'project';
	protected static $_primary_key = array('id');
	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => false
		)
	);
	protected static $_belongs_to = array(
	    'owner' => array(
	        'key_from' => 'owner_id',
	        'model_to' => 'Model_User',
	        'key_to' => 'id',
	        'cascade_save' => false,
	        'cascade_delete' => false,
	    )
	);
	protected static $_has_many = array(
	    'workunits' => array(
	        'key_from' => 'id',
	        'model_to' => 'Model_Workunit',
	        'key_to' => 'project_id',
	        'cascade_save' => false,
	        'cascade_delete' => false,
	    )
	);
	
	
	/*
	 * Loads a project by ID
	 *
	 * @param	$project_id	int		The ID of the project to load
	 * @return	Object
	 */
	public static function load_by_id($project_id)
	{
		return self::query()->where('id', $project_id)->get_one();
	}
	
	
	/*
	 * Adds new project to the DB (delegates to static::handle_save for the grunt work)
	 *
	 * @return	Object
	 */
	public static function add()
	{
		$project = self::forge();
		$project->owner_id = Model_User::identity_id();
		$project->color = App_Misc::create_color();
		$project = self::handle_save($project);
		
		return $project;
	}
	
	
	/*
	 * Edits a project (delegates to static::handle_save for the grunt work)
	 *
	 * @param	$project		object		An instance of the project to save
	 * @return	Object
	 */
	public static function edit($project)
	{
		return self::handle_save($project);
	}
	
	
	/*
	 * Handles validation and saving of projects
	 *
	 * @param	$project		object		An instance of the project to save
	 * @return	Object
	 */
	private static function handle_save($project)
	{
		$val = App_Val::forge('project/project');
		if ($val) {
			$project->name = $val->validated('name');
			$project->save();
			
			return $project;
		}
		
		return false;
	}
	
	
	/*
	 * Destroys a project and any workunits that belong to it
	 *
	 * @return	Bool
	 */
	public function destroy()
	{
		foreach($this->workunits as $workunit) {
			$workunit->destroy();
		}
		$this->workunits = NULL;
		
		return $this->delete();
	}
	
	
	/*
	 * Gets all of a projects workunits 
	 *
	 * @return	array
	 */
	public function getWorkunits()
	{
		$ts = Date::forge()->get_timestamp();
		$qs = '(SELECT SUM(IFNULL(entry.end_time,'.$ts.') - entry.start_time) FROM entry WHERE entry.workunit_id = workunit.id)';
		$st = '(SELECT MAX(entry.start_time) FROM entry WHERE entry.workunit_id = workunit.id)';
		$qm = DB::query('SELECT workunit.*, '.$qs.' AS time, '.$st.' AS last_started FROM workunit WHERE workunit.project_id = ' . $this->id)->as_object('Model_Workunit')->execute()->as_array('id');
		
		return $qm;
	}
	
	
	/*
	 * Generates a new color for a project
	 *
	 * @return	object
	 */
	public function new_color()
	{
		$this->color = App_Misc::create_color();
		$this->save();
		
		return $this;
	}
	
	
}