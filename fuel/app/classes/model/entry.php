<?php

class Model_Entry extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'workunit_id',
		'start_time',
		'end_time'
	);
	protected static $_table_name = 'entry';
	protected static $_primary_key = array('id');
	protected static $_belongs_to = array(
	    'workunit' => array(
	        'key_from' => 'workunit_id',
	        'model_to' => 'Model_Workunit',
	        'key_to' => 'id',
	        'cascade_save' => false,
	        'cascade_delete' => false,
	    )
	);
	
	
	/*
	 * Loads an entry by ID
	 *
	 * @param	$entry_id	int		The ID of the entry to load
	 * @return	Object
	 */
	public static function load_by_id($entry_id)
	{
		return self::query()->where('id', $entry_id)->get_one();
	}
	
	
	/*
	 * Loads active entries
	 *
	 * @param	$workunit_id	int		The parent workunit ID
	 * @return	array
	 */
	public static function load_active($workunit_id)
	{
		return self::query()->where('workunit_id', $workunit_id)->where('end_time', NULL)->get_one();
	}
	
	
	/*
	 * Adds an entry
	 *
	 * @param	$workunit_id	int		The ID of the parent workunit
	 * @param	$end			int		Optional end time for the entry
	 * @return	Object
	 */
	public static function add($workunit_id, $end = NULL)
	{
		$active = static::load_active($workunit_id);
		if ($active) {
			static::end($workunit_id);
		}
		
		$ts = Date::forge()->get_timestamp();
		$entry = self::forge();
		$entry->workunit_id = $workunit_id;
		$entry->start_time = $ts;
		
		if ($end == true) {
			$entry->end_time = $ts;
		}
		
		$entry->save();
		
		return $entry;
	}
	
	
	/*
	 * Ends a active workunit timer
	 *
	 * @param	$workunit_id	int		The workunit to end a timer for
	 * @return	Object
	 */
	public static function end($workunit_id)
	{
		$active = static::load_active($workunit_id);
		if ($active) {
			$active->end_time = Date::forge()->get_timestamp();
			$active->save();
		}
		
		return $active;
	}
	
	
}