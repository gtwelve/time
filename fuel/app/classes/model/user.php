<?php

class Model_User extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'first_name',
		'last_name',
		'email',
		'password',
		'group',
		'login_hash',
		'status',
		'activation_key',
		'password_reset_key',
		'password_reset_expiry',
		'last_login',
		'activated_at',
		'created_at',
		'updated_at'
	);
	protected static $_table_name = 'users';
	protected static $_primary_key = array('id');
	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => false
		)
	);
	protected static $_has_many = array(
	    'projects' => array(
	        'key_from' => 'id',
	        'model_to' => 'Model_Project',
	        'key_to' => 'owner_id',
	        'cascade_save' => true,
	        'cascade_delete' => false,
	    )
	);
	
	
	public static $active_identity = NULL;
	public static $active_identity_calls = 0;
	
	const AC_ACTIVE = 1000;
	const AC_AWAITING_ACTIVATION = 1001;
	const AC_AWAITING_RESET = 1002;
	const AC_CLOSED = 1003;
	
	
	
	/* 
	 * Loads a user by ID
	 *
	 * @param	$user_id	int		The ID of the user to load
	 * @return	Object
	 */
	public static function load_by_id($user_id)
	{
		return Model_User::query()->where('id', $user_id)->get_one();
	}
	
	
	/* 
	 * Loads a user by email address
	 *
	 * @param	$email_address	string		The email address of the user to load
	 * @return	Object
	 */
	public static function load_by_email($email_address)
	{
		return Model_User::query()->where('email', $email_address)->get_one();
	}
	
	
	/* 
	 * Gets the model of the logged in user
	 *
	 * @param	$force_check	bool		If true it wont return a cached result
	 * @return	Object
	 */
	public static function identity($force_check = false)
	{
		if (self::$active_identity_calls == 0 || $force_check == true) {
			self::$active_identity_calls++;
			self::$active_identity = Auth::check() ? Model_User::forge(Auth::get_user()) : NULL ;
			View::set_global('identity', self::$active_identity);
			return self::$active_identity;
		} else {
			self::$active_identity_calls++;
			View::set_global('identity', self::$active_identity);
			return self::$active_identity;
		}
	}
	
	
	/* 
	 * Gets the model id of the logged in user
	 *
	 * @param	$force_check	bool		If true it wont return a cached result
	 * @return	Object
	 */
	public static function identity_id($force_check = false)
	{
		$identity = self::identity($force_check);
		return is_null($identity) ? NULL : $identity->id ;
	}
	
	
	
	/* 
	 * Trys to login a user
	 *
	 * @return	Bool
	 */
	public static function login()
	{
		$val = App_Val::forge('auth/login');
		if ($val) {
			$login_result = Auth::instance()->login($val->validated('email'), $val->validated('password'));
			if ($login_result) {
				$user = Model_User::identity(true);
				
				if (Input::post('remmeber', 'no') == 'yes') { Auth::remember_me(); } else { Auth::dont_remember_me(); }
				
				if ($user->status == self::AC_ACTIVE) {
					return true;
				} else {
					Model_User::logout();
					
					if ($user->status == self::AC_AWAITING_RESET) {
						App_Msg::add('account awaiting password reset', 'error');
						App_Msg::add('you can <a href="/auth/resend/' . Crypt::encode($user->id, 'resend_code') . '">resend your email here</a>', 'info', false);
					} elseif ($user->status == self::AC_AWAITING_ACTIVATION) {
						Model_User::logout();
						App_Msg::add('awaiting account activation', 'error');
						App_Msg::add('you can <a href="/auth/resend/' . Crypt::encode($user->id, 'resend_code') . '">resend your email here</a>', 'info', false);
					} elseif ($user->status == self::AC_CLOSED) {
						Model_User::logout();
						App_Msg::add('acount closed, you cannot access it anymore', 'error');
					}
					
					return false;
				}
			}
			
			App_Msg::add('We could not find a user with that email and password. Please try again.', 'error');
			return false;
		} else { return false; }
	}
	
	
	/* 
	 * force logs in a user
	 *
	 * @param	$user_id	int		The user ID to login
	 * @return	Object
	 */
	public static function force_login($user_id)
	{
		return Auth::instance()->force_login($user_id);
	}
	
	
	/* 
	 * Logs out a user
	 *
	 * @return	Bool
	 */
	public static function logout()
	{
		return Auth::logout();
	}
	
	
	/* 
	 * Registers a user with a validation check
	 *
	 * @param	$group					int		The group to add the user to
	 * @param	$require_activation		bool	If the new user is required to go through the activation process
	 * @return	Int
	 */
	public static function register_user($group = 1, $require_activation = false)
	{
		$val = App_Val::forge('auth/register');
		if ($val) {
			$user_meta = array(
				'first_name' => $val->validated('first_name'),
				'last_name' => $val->validated('last_name')
			);
			
			$user_id = Model_User::create_user($val->validated('email'), $val->validated('password'), $group, $require_activation, $user_meta);
			if ($user_id) {
				if ($require_activation) {
					return $user_id;
				} else {
					return Model_User::force_login($user_id);
				}
			}
			
			App_Msg::add('unable to register for some reason', 'error');
			return false;
		} else { return false; }
	}
	
	
	/* 
	 * Creates a user
	 *
	 * @params	Self explanatory
	 * @return	Int
	 */
	public static function create_user($email, $password, $group = 1, $use_activation = false, $user_meta = NULL)
	{
		$profile = array('email' => $email, 'password' => $password, 'group' => $group);
		
		if ($use_activation == true) { $profile['activation_key'] = Str::random('unique'); $profile['status'] = self::AC_AWAITING_ACTIVATION; } else { $profile['status'] = self::AC_ACTIVE; }
		if (!is_null($user_meta)) { $profile = array_merge($user_meta, $profile); }
		
		return Auth::instance()->create_user($profile);
	}
	
	
	/* 
	 * Edits a users account
	 *
	 * @return	Bool
	 */
	public static function edit_account()
	{
		$user = Model_User::identity();
		if ($user) {
			$val = App_Val::forge('auth/edit_account');
			if ($val) {
				$values = array(
					'email' => $val->validated('email'),
					'first_name' => $val->validated('first_name'),
					'last_name' => $val->validated('last_name')
				);
				
				Model_User::update_user($values, $user->id);
				Model_User::identity(true);	// reload identity
				App_Msg::add('account updated', 'success');
				return true;
			} else { return false; }
		} else {
			App_Msg::add('updateable user not found', 'error');
			return false;
		}
	}
	
	
	/* 
	 * Edits a user spassword
	 *
	 * @return	Bool
	 */
	public static function edit_password()
	{
		$user = Model_User::identity();
		if ($user) {
			$val = App_Val::forge('auth/edit_account_password');
			if ($val) {
				$changed = Auth::instance()->change_password($val->validated('old_password'), $val->validated('password'), $user->id);
				
				if ($changed) {
					App_Msg::add('password updated', 'success');
					return true;
				} else {
					App_Msg::add('old password was incorrect', 'error');
					return false;
				}
			} else { return false; }
		} else {
			App_Msg::add('updateable user not found', 'error');
			return false;
		}
	}
	
	
	/* 
	 * Updates a user (delegates to simpleauth for the gunt work)
	 *
	 * @return	Bool
	 */
	public static function update_user($values, $user_id)
	{
		return Auth::instance()->update_user($values, $user_id);
	}
	
	
	/* 
	 * Hnadles forgot password logic
	 *
	 * @return	Bool
	 */
	public static function password_forgot()
	{
		$val = App_Val::forge('auth/forgot');
		if ($val) {
			$user = Model_User::load_by_email($val->validated('email'));
			
			if ($user) {
				if ($user->status == self::AC_AWAITING_ACTIVATION) {
					App_Msg::add('your account is still awaiting activation', 'error');
					App_Msg::add('you can <a href="/auth/resend/' . Crypt::encode($user->id, 'resend_code') . '">resend your email here</a>', 'info', false);
					return false;
				} elseif ($user->status == self::AC_CLOSED) {
					App_Msg::add('your account has been closed. you cannot reset your password', 'error');
					return false;
				} else {
					Auth::instance()->reset_password($user->id);
					
					$reset_key = Str::random('unique');
					$profile_update = array('status' => self::AC_AWAITING_RESET, 'password_reset_key' => $reset_key, 'password_reset_expiry' => Date::forge()->get_timestamp()+172800);
					$update = Model_User::update_user($profile_update, $user->id);
					
					return ($update) ? $reset_key : array('unable to reset account. please try again.') ;
				}
			} else {
				App_Msg::add('user not found', 'error');
				return false;
			}
		} else { return false; }
	}
	
	
	/* 
	 * Handles reset password logic
	 *
	 * @params	$user_id 	int			The user ID to reset
	 * @params	$reset_key 	string		The reset key
	 * @return	Bool
	 */
	public static function password_reset($user_id, $reset_key)
	{
		$valid_link = Model_User::check_password_reset_link($user_id, $reset_key);
		
		if ($valid_link) {
			$val = App_Val::forge('auth/forgot_reset');
			if ($val) {
				$user = Model_User::load_by_id($user_id);
				
				$tmp_password = Auth::instance()->reset_password($user->id);
				$profile_update = array('status' => self::AC_ACTIVE, 'password_reset_key' => NULL, 'old_password' => $tmp_password, 'password' => $val->validated('password'));
				$update = Model_User::update_user($profile_update, $user->id);
				$login = Model_User::force_login($user->id);
				
				return $update;
			} else { return false; }
		} else { return false; }
	}
	
	
	/* 
	 * Checks to see if password reset params are valid
	 *
	 * @params	$user_id 	int			The user ID to reset
	 * @params	$reset_key 	string		The reset key
	 * @return	Bool
	 */
	public static function check_password_reset_link($user_id, $reset_key)
	{
		$user = Model_User::load_by_id($user_id);
		if ($user && ($user->password_reset_key === $reset_key) && (Date::forge()->get_timestamp() < $user->password_reset_expiry)) {
			return true;
		} else {
			throw new HttpNotFoundException;
		}
	}
	
	
	/* 
	 * Activates a user
	 *
	 * @params	$user_id 			int			The user ID to reset
	 * @params	$activation_code 	string		A valid activation code
	 * @return	Bool
	 */
	public static function activate_user($user_id, $activation_code)
	{
		$user = Model_User::load_by_id($user_id);
		if ($user) {
			if ($user->activation_key === $activation_code) {
				$activate = array('activation_key' => NULL, 'status' => self::AC_ACTIVE, 'activated_at' => \Date::forge()->get_timestamp());
				Model_User::force_login($user->id);
				Model_User::update_user($activate, $user_id);
				return true;
			} else { return false; }
		}
		
		return false;
	}
	
	
	/* 
	 * NOTICE: THe function is yet to be completed, emails are not sent
	 * Resends a users activation/forgot password email
	 *
	 * @params	$resend_code 			int			The resend code of the user
	 * @return	Bool
	 */
	public static function resend_email($resend_code)
	{
		$user_id = Crypt::decode($resend_code, 'resend_code');
		$user = Model_User::load_by_id($user_id);
		
		if ($user) {
			if ($user->status == self::AC_AWAITING_ACTIVATION) {
				
				return true;	// sent activation email
				
			} elseif ($user->status == self::AC_AWAITING_RESET) {
				
				return true;	// sent reset email
				
			} else { return false; }
		} else { return false; }
	}
	
	
	/* 
	 * Concats a user first and last names
	 *
	 * @return	String
	 */
	public function full_name() {
		return $this->first_name . ' ' . $this->last_name;
	}
	
	
	/* 
	 * Gets all of a user projects
	 *
	 * @return	array
	 */
	public function get_projects()
	{
		return $this->projects;
	}
	
	
}