<?php

namespace Fuel\Tasks;

class CreateUser
{
	/**
	 * Creates an example user.
	 *
	 * @return null
	 */
	public static function run()
	{
		$user_id = \Model_User::create_user('example@example.com', '123', 100, false, array('first_name' => 'Example', 'last_name' => 'User'));
		
		$project = \Model_Project::forge();
		$project->owner_id = $user_id;
		$project->color = \App_Misc::create_color();
		$project->name = 'Default project';
		$project->save();
	}
	
}

/* End of file tasks/create_user.php */
