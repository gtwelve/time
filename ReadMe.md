## Overview
Time is a very simple app that lets you create an unlimited number of timers to record events. You can also separate a collection of timers into projects. The project is built in FuelPHP and backbone.

## Installation
1. Clone the repository: `git clone  git://git@bitbucket.org:gtwelve/time.git`
2. Run `composer install` (assuming you have composer installed globally)
3. Import the database schema from `db/schema.sql` as `time_dev`
4. Run `bash install` to set permissions on folders and create a user
5. For production create a file at `fuel/app/config/production/db.php` with your DB settings. See example at the bottom of the page.
6. Login with the username: example@example.com and password: 123

DB example:
~~~~
<?php
return array(
	'default' => array(
		'connection'  => array(
			'dsn'        => 'mysql:host=127.0.0.1;dbname=time',
			'username'   => 'root',
			'password'   => ''
		),
		'profiling' => true
	)
);
~~~~